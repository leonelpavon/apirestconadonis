'use strict'

const ProyectoController = require('../app/Controllers/Http/ProyectoController')
const UserController = require('../app/Controllers/Http/UserController')

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})

Route.group (() => { 
Route.post('usuarios/registro','UserController.store'); 
Route.post('usuarios/login','UserController.login'); 

//Rutas de los proyectos 

Route.get('proyectos', 'ProyectoController.index');
Route.post('proyectos', 'ProyectoController.create');
Route.delete('proyectos/:id', 'ProyectoController.destroy'); 
Route.patch('proyectos/:id', 'ProyectoController.update'); 
 
//Rutas de las tareas 
Route.get('proyectos/:id/tareas', 'TareaController.index'); 
Route.post('proyectos/:id/tareas', 'TareaController.create'); 
Route.delete('tareas/:id', 'ProyectoController.destroy'); 
Route.patch('tareas/:id', 'ProyectoController.update'); 
}).prefix('apiv1/').middleware('auth'); 